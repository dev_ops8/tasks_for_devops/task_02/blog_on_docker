## Task_02
### Description of the task
Our web-site from previous task. We must think what can happen if hundred and thousand people
go to our web-site > it will fall! We must create about 3 instances for back and front for our web-site to prod!
#### Check-points:
-  Docker-compose up 2-back, 3-front, db, nginx as a balancer
-  Create Healthcheck for all containers  
-  Create CI pipe
-  If we have commit in dev-branch, we must update apps
### Result of job
- 1 step: Create docker-compose, that deploy our environment with db, 2-back, 3-front, nginx
- 2 step: Environment have round-robin balance princip
- 3 step: as a result we can receive static files of site from front or can get api-request from back